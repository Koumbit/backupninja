# This class manages the default backupninja cron task.
#
# @private
#
class backupninja::cron {

  assert_private()

  if $backupninja::manage_cron {

    backupninja::cron::task { 'default':
      ensure => $backupninja::cron_ensure,
      file   => $backupninja::cron_file,
      min    => $backupninja::cron_min,
      hour   => $backupninja::cron_hour,
      dom    => $backupninja::cron_dom,
      month  => $backupninja::cron_month,
      dow    => $backupninja::cron_dow,
    }

  }

}
