# Run rdiff-backup as part of a backupninja run.
#
define backupninja::action::rdiff (
  String $ensure                    = 'present',
  Integer $order                    = 90,
  Optional[Boolean] $validate       = undef,
  # general handler config
  Optional[String] $when            = undef,
  String $options                   = '--force',
  Integer $nicelevel                = 0,
  Boolean $testconnect              = true,
  Optional[Integer] $bwlimit        = undef,
  Boolean $ignore_version           = false,
  # [source]
  Enum['local', 'remote'] $src_type = 'local',
  Optional[String] $label           = undef,
  String $keep                      = '30',
  Optional[Array[String]] $include  = undef,
  Optional[Array[String]] $exclude  = undef,
  Optional[String] $src_host        = undef,
  Optional[String] $src_user        = undef,
  # [dest]
  Enum['local', 'remote'] $type     = 'remote',
  Stdlib::Absolutepath $directory   = "${backupninja::_account_home}/${name}",
  Optional[String] $host            = $backupninja::account_host,
  Optional[String] $user            = $backupninja::account_user,
  Optional[String] $sshoptions      = undef,
) {

  # install client dependencies
  if $backupninja::manage_packages {
    ensure_packages(['rdiff-backup'], {'ensure' => $backupninja::ensure_rdiffbackup_version})
    if $bwlimit {
      ensure_packages(['cstream'], {'ensure' => 'installed'})
    }
  }

  if $src_type == 'remote' {
    if empty($src_host) or empty($src_user) {
      fail('Must define src_host and src_user for remote backup!')
    }
    $_src_host = $src_host
    $_src_user = $src_user
  }
  else {
    $_src_host = undef
    $_src_user = undef
  }

  if $type == 'remote' {
    if empty($host) or empty($user) {
      fail('Must define host and user for remote backup!')
    }
    $_host = $host
    $_user = $user
  }
  else {
    $_host = undef
    $_user = undef
  }

  if empty($include) {
    fail('Must include one or more directories to backup!')
  }

  # $backupninja::account_home was empty and default used
  if $directory == "/${name}" {
    fail('Must define a directory for backup!')
  }

  $config = {
    'when'           => $when,
    'options'        => $options,
    'nicelevel'      => $nicelevel,
    'testconnect'    => $testconnect,
    'bwlimit'        => $bwlimit,
    'ignore_version' => $ignore_version,
    'source'         => {
      'type'    => $src_type,
      'host'    => $_src_host,
      'user'    => $_src_user,
      'label'   => $label,
      'keep'    => $keep,
      'include' => $include,
      'exclude' => $exclude,
    },
    'dest'           => {
      'type'       => $type,
      'directory'  => $directory,
      'host'       => $_host,
      'user'       => $_user,
      'sshoptions' => $sshoptions,
    },
  }

  backupninja::action { $name:
    ensure   => $ensure,
    order    => $order,
    type     => 'rdiff',
    content  => epp('backupninja/action.epp', { 'config' => $config }),
    validate => $validate,
  }

}
