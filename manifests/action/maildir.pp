# Maildir backup usying rsync, as part of a backupninja run.
#
# @param src
#  The absolute path of the source destination to sync
#
# @param dest
#  The absolute path of the destination directory to sync to
#
# @param ensure
#   Whether the backup action should be present, absent or disabled. When
#   removing or disabling an action, the type and order must match the file to
#   act upon. A disabled action configuration will be kept on the filesystem but
#   will always be ignored by backupninja.
#
# @param order
#   Determine the order prefix which will be applied to the configuration
#   filename. Must be a positive integer.
#
# @param when
#   The time at which the action should be run, eg. 'everyday at 7:00'
#
# @param validate
#   Whether to test the backup action before installing it. The test is run
#   with `backupninja --test --now --run`. Be aware that in the case of actions
#   configuration for remote backup, it will attempt to connect to the remote
#   host. If the test fails, the backup action configuration will not be
#   installed/updated.
#
# @param keepdaily
#   The number of daily folders to keep. These will be rotated every day
#   (assuming the job is run every day) and there will be daily.X folders
#   up to the amount of $keepdaily. Note: keepdaily should not exceed 7,
#   otherwise the daily.8 is rotated into weekly.1 and the time spread
#   is inconsistent.
#
# @param keepweekly
#   Similar to keepdaily, but for weeks. Should not exceed 4, for the the
#   same reason as keepdaily.
#
# @param keepmontly
#   Similar to keepdaily, but for months.
#
# @param host
#   The host towards which the maildirs will be synchonized.
#
# @param user
#   The user to use for the maildir synchronization.
#
# @param port
#   The ssh port on the destination to connect to.
#
# @param destid_file
#   An optional absolute path for a private key file which is used for the
#   ssh connection towrdas the host.
#
# @param ssh_options
#   Any options to be passed to SSH in strin format.
#
# @param remove_maildirs
#   When set to true, maildirs that have been deleted on the source will be removed
#   on the destination.
#
# @param multiconnection
#   Setting for ssh-mux to re-use existing SSH connections.
#   @see http://www.debian-administration.org/articles/290
#   This setting may required further configuration to be done.
#
# @param srcsubdirs
#   An optional list of sub-directories that the $src directory is
#   expected to contain. Each of these sub-directories should contain
#   the Maildirs that start with the string.
#   eg. '0 1 2 a b c'
#
define backupninja::action::maildir (
  Stdlib::AbsolutePath $src,
  Stdlib::AbsolutePath $dest,
  String $ensure                              = 'present',
  Integer $order                              = 99,
  Optional[String] $when                      = undef,
  Optional[Boolean] $validate                 = undef,
  Integer $keepdaily                          = 7,
  Integer $keepweekly                         = 4,
  Integer $keepmonthly                        = 6,
  Optional[String] $host                      = $backupninja::account_host,
  Optional[String] $user                      = $backupninja::account_user,
  Optional[Integer] $port                     = 22,
  Optional[Stdlib::AbsolutePath] $destid_file = undef,
  Optional[String] $ssh_options               = undef,
  Optional[Boolean] $remove_maildirs          = undef,
  Optional[String] $multiconnection           = undef,
  Optional[String] $srcsubdirs         = undef,
) {

  if $keepdaily > 7 {
    warning('$keepdaily should not be larger than 7, otherwise inconsistent backup rotations are produced.
  @see https://0xacab.org/riseuplabs/backupninja/-/issues/11301')
  }
  if $keepweekly > 4 {
    warning('$keepweekly should not be larger than 4, otherwise inconsistent backup rotations are produced.
  @see https://0xacab.org/riseuplabs/backupninja/-/issues/11301')
  }

  $config = {
    'when'            => $when,
    'keepdaily'       => $keepdaily,
    'keepweekly'      => $keepweekly,
    'keepmonthly'     => $keepmonthly,
    'srcdir'          => $src,
    'srcsubdirs'      => $srcsubdirs,
    'destdir'         => $dest,
    'desthost'        => $host,
    'destuser'        => $user,
    'destport'        => $port,
    'destid_file'     => $destid_file,
    'sshoptions'      => $ssh_options,
    'remove'          => $remove_maildirs,
    'multiconnection' => $multiconnection,
  }

  backupninja::action { $name:
    ensure   => $ensure,
    order    => $order,
    type     => 'maildir',
    content  => epp('backupninja/action.epp', { 'config' => $config }),
    validate => $validate,
  }
}
