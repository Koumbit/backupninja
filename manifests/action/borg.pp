# Run borgbackup as part of a backupninja run.

define backupninja::action::borg (
  String $ensure                                  = 'present',
  Integer $order                                  = 90,
  Optional[Boolean] $validate                     = false,
  # general handler config
  Optional[String] $when                          = undef,
  Integer $ionicelevel                            = 5,
  Integer $nicelevel                              = 0,
  Boolean $testconnect                            = true,
  Optional[Integer] $bwlimit                      = undef,
  # [source]
  Boolean $init                                   = true,
  Array $include                                  = [ ],
  Optional[Array] $exclude                        = undef,
  Optional[String] $create_options                = undef,
  Boolean $prune                                  = true,
  String $keep                                    = '30d',
  Optional[String] $prune_options                 = undef,
  Optional[Stdlib::Absolutepath] $cache_directory = undef,
  # [dest]
  Stdlib::Absolutepath $directory                 = "${backupninja::_account_home}/${name}",
  Optional[String] $host                          = $backupninja::account_host,
  Optional[String] $user                          = $backupninja::account_user,
  String $archive                                 = '{now:%Y-%m-%dT%H:%M:%S}',
  String $compression                             = 'lz4',
  Backupninja::Borg::Encryption_mode $encryption  = 'none',
  Optional[String] $passphrase                    = undef,
) {

  # install client dependencies
  if $backupninja::manage_packages {
    ensure_packages(['borgbackup'], {'ensure' => $backupninja::ensure_borgbackup_version})
  }

  $_user = $host ? {
    'localhost' => undef,
    default     => $user,
  }

  if empty($include) {
    fail('Must define one or more directories to backup!')
  }

  $config = {
    'when'        => $when,
    'ionicelevel' => $ionicelevel,
    'nicelevel'   => $nicelevel,
    'testconnect' => $testconnect,
    'bwlimit'     => $bwlimit,
    'source'      => {
      'init'            => $init,
      'include'         => $include,
      'exclude'         => $exclude,
      'create_options'  => $create_options,
      'prune'           => $prune,
      'keep'            => $keep,
      'prune_options'   => $prune_options,
      'cache_directory' => $cache_directory,
    },
    'dest'        => {
      'directory'   => $directory,
      'host'        => $host,
      'user'        => $_user,
      'archive'     => $archive,
      'compression' => $compression,
      'encryption'  => $encryption,
      'passphrase'  => $passphrase,
    },
  }

  backupninja::action { $name:
    ensure   => $ensure,
    order    => $order,
    type     => 'borg',
    content  => epp('backupninja/action.epp', { 'config' => $config }),
    validate => $validate,
  }

}
